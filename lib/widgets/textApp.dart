import 'package:flutter/material.dart';

class TextApp extends StatelessWidget {
  double size;
  bool isFontweight;
  final String text;
  final Color color;

  TextApp(
      {Key? key,
      this.size = 16,
      required this.text,
      this.color = Colors.white,
      this.isFontweight = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
          color: color,
          fontSize: size,
          fontWeight:
              isFontweight == true ? FontWeight.bold : FontWeight.normal),
    );
  }
}
