import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void changeScreenCupertino(BuildContext context, Widget widget) {
  Navigator.push(
      context,
      CupertinoPageRoute(
          builder: (context) => widget,
          settings: RouteSettings(name: 'HomeView')));
}

void changeScreenReplacementCupertino(BuildContext context, Widget widget) {
  Navigator.pushReplacement(
      context, CupertinoPageRoute(builder: (context) => widget));
}

void changeScreenAllReplacementCupertino(
    BuildContext context, Widget widget, String route) {
  Navigator.pushAndRemoveUntil(
    context,
    CupertinoPageRoute(builder: (context) => widget),
    ModalRoute.withName("/$route"),
  );
}

void changeScreenMaterial(BuildContext context, Widget widget) {
  Navigator.push(context, MaterialPageRoute(builder: (context) => widget));
}

void changeScreenReplacementMaterial(BuildContext context, Widget widget) {
  Navigator.pushReplacement(
      context, CupertinoPageRoute(builder: (context) => widget));
}

void changeScreenAllReplacementMaterial(
    BuildContext context, Widget widget, String route) {
  Navigator.pushAndRemoveUntil(
    context,
    CupertinoPageRoute(builder: (context) => widget),
    ModalRoute.withName("/$route"),
  );
}
