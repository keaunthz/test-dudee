import 'package:flutter/material.dart';
import 'package:test_dudee/helpers/utils/screen_navigator.dart';
import 'package:test_dudee/screen/selectWM.dart';
import 'package:test_dudee/widgets/textApp.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF12A8D5),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        centerTitle: true,
        elevation: 0,
        title: TextApp(
          text: "หน้าหลัก",
          size: 20,
          isFontweight: true,
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(25.0),
            child: TextApp(
              text: "เครื่องซักผ้าทั้งหมด",
              size: 20,
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.fromLTRB(10, 40, 10, 0),
              decoration: BoxDecoration(
                color: Colors.white70,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemCount: 5,
                itemBuilder: (BuildContext context, int index) => index != 3 &&
                        index != 4
                    ? GestureDetector(
                        onTap: () {
                          changeScreenCupertino(
                              context,
                              SelectWM(
                                  name:
                                      "เครื่องซักผ้าเครื่องที่ ${index + 1}"));
                          print(index);
                        },
                        child: Card(
                          margin: EdgeInsets.only(bottom: 15),
                          elevation: 3,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Image.asset(
                                      "assets/icon/washingMachine.png",
                                      height: 40,
                                    ),
                                    SizedBox(
                                      width: 15,
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        TextApp(
                                          text:
                                              "เครื่องซักผ้าเครื่องที่ ${index + 1}",
                                          color: Colors.black,
                                        ),
                                        TextApp(
                                          text: "พร้อมให้บริการ",
                                          color: Colors.green,
                                          size: 14,
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                Icon(Icons.arrow_forward_ios_outlined)
                              ],
                            ),
                          ),
                        ),
                      )
                    : Card(
                        margin: EdgeInsets.only(bottom: 15),
                        elevation: 3,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12.0),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  Image.asset(
                                    "assets/icon/washingMachineNu.png",
                                    height: 40,
                                  ),
                                  SizedBox(
                                    width: 15,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      TextApp(
                                        text:
                                            "เครื่องซักผ้าเครื่องที่ ${index + 1}",
                                        color: Colors.grey.shade600,
                                      ),
                                      TextApp(
                                        text: "ไม่พร้อมให้บริการ",
                                        color: Colors.grey,
                                        size: 14,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              Icon(
                                Icons.arrow_forward_ios_outlined,
                                color: Colors.grey,
                              )
                            ],
                          ),
                        ),
                      ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
