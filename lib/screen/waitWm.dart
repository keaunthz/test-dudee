import 'dart:async';

import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:test_dudee/helpers/utils/screen_navigator.dart';
import 'package:test_dudee/screen/homePage.dart';
import 'package:test_dudee/widgets/textApp.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class WaitWm extends StatefulWidget {
  final String name;
  int start;

  WaitWm({Key? key, required this.start, required this.name}) : super(key: key);

  @override
  State<WaitWm> createState() => _WaitWmState();
}

class _WaitWmState extends State<WaitWm> {
  late Timer _timer;
  late SharedPreferences pref;
  @override
  void initState() {
    startTimer();
    super.initState();
  }

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (widget.start < 1) {
            timer.cancel();
          } else {
            setState(() {
              widget.start = widget.start - 1;
              if (widget.start == 60) {
                lineNotify();
              }
            });
          }
        },
      ),
    );
  }

  void lineNotify() async {
    var headers = {
      'Authorization': 'Bearer 5xRkG5B294n6LkZgNRe3iZF8yAdtkl19SOWla8ORrYz',
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    var request = http.Request(
        'POST', Uri.parse('https://notify-api.line.me/api/notify'));
    request.bodyFields = {
      'message': '${widget.name} กำลังจะซักเสร็จภายใน 1 นาที'
    };
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
    } else {
      print(response.reasonPhrase);
    }
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  Widget build(BuildContext context) {
    MediaQueryData queryData = MediaQuery.of(context);
    double screenWidth = queryData.size.width;
    double screenHeight = queryData.size.height;
    // int minutes = widget.start ~/ 60;
    int minutes = (widget.start ~/ 60) % 60;
    int seconds = widget.start % 60;
    return Scaffold(
      backgroundColor: Color(0xFF12A8D5),
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        elevation: 0,
        title: TextApp(
          text: "การซักผ้า",
          size: 20,
          isFontweight: true,
        ),
      ),
      body: DoubleBackToCloseApp(
        snackBar: SnackBar(
          content: Text("กดกลับอีกครั้งเพื่อออกจากแอพ"),
        ),
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.all(15),
              width: screenWidth,
              // height: screenHeight * 0.3,
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: widget.start > 0
                        ? Image.asset(
                            "assets/icon/washingMachine.png",
                            height: 180,
                          )
                        : Image.asset(
                            "assets/icon/2150.png",
                            height: 250,
                          ),
                  ),
                  TextApp(
                    text: widget.start > 0
                        ? "เครื่องกำลังซักผ้าให้คุณ..."
                        : "ซักผ้าเสร็จแล้ว!",
                    color: Colors.black,
                    isFontweight: true,
                    size: 18,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  widget.start > 0
                      ? SizedBox(
                          width: 100,
                          height: 100,
                          child: Stack(
                            fit: StackFit.expand,
                            children: [
                              CircularProgressIndicator(
                                value: widget.start / 120,
                                valueColor:
                                    AlwaysStoppedAnimation(Colors.indigo[50]),
                                // color: Colors.yellow,
                                strokeWidth: 12,
                                backgroundColor: Color(0xFF12A8D5),
                              ),
                              Center(
                                child: TextApp(
                                  text:
                                      "${minutes.toString().padLeft(2, '0')} : ${seconds.toString().padLeft(2, '0')}",
                                  color: Colors.black,
                                ),
                              ),
                            ],
                          ),
                        )
                      : Container(),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      Flexible(
                        child: TextApp(
                          text: widget.start > 0
                              ? "ระบบจะทำการแจ้งเตือนคุณผ่าน Line หากมีการซักผ้าใกล้เสร็จแล้ว"
                              : "กรุณาตรวจสอบเสื้อผ้าและอุปกรณ์ซักผ้าก่อนกดปุ่มเสร็จสิ้น ขอบคุณที่ใช้บริการ",
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: screenHeight * 0.05,
            ),
            widget.start > 0
                ? Container()
                : ElevatedButton(
                    child: TextApp(
                      text: "เสร็จสิ้น",
                      isFontweight: true,
                    ),
                    style: ButtonStyle(
                      padding: MaterialStateProperty.all<EdgeInsets>(
                          EdgeInsets.fromLTRB(30, 15, 30, 15)),
                      foregroundColor:
                          MaterialStateProperty.all<Color>(Colors.white),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Color(0xFF39CD07)),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15),
                        ),
                      ),
                    ),
                    onPressed: () async {
                      changeScreenAllReplacementCupertino(
                          context, HomePage(), "homePage");
                    })
          ],
        ),
      ),
    );
  }
}
