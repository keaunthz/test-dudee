import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_dudee/helpers/utils/screen_navigator.dart';
import 'package:test_dudee/screen/homePage.dart';
import 'package:test_dudee/widgets/textApp.dart';
import 'package:url_launcher/url_launcher.dart';

class JoinLinePage extends StatefulWidget {
  const JoinLinePage({Key? key}) : super(key: key);

  @override
  _JoinLinePageState createState() => _JoinLinePageState();
}

class _JoinLinePageState extends State<JoinLinePage> {
  late SharedPreferences pref;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: DoubleBackToCloseApp(
          snackBar: SnackBar(
            content: Text("กดกลับอีกครั้งเพื่อออกจากแอพ"),
          ),
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  TextApp(
                    text: "Test Dudee",
                    color: Colors.black,
                    size: 26,
                    isFontweight: true,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(22.0),
                    child: CircleAvatar(
                      radius: 90, // Image radius
                      backgroundImage: AssetImage("assets/icon/wmicons.jpg"),
                    ),
                  ),
                  Text(
                    "หากคุณต้องการรับการแจ้งเตือนการซักผ้า กรุณากด \"เข้าร่วม\" เพื่อรับการแจ้งเตือน",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  ElevatedButton(
                    child: TextApp(
                      text: "เข้าร่วม",
                      isFontweight: true,
                      color: Color(0xFF39CD07),
                    ),
                    style: ButtonStyle(
                      padding: MaterialStateProperty.all<EdgeInsets>(
                          EdgeInsets.all(12)),
                      foregroundColor:
                          MaterialStateProperty.all<Color>(Color(0xFF39CD07)),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.white),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15),
                            side: BorderSide(color: Color(0xFF39CD07))),
                      ),
                    ),
                    onPressed: () {
                      launch('https://line.me/R/ti/g/cbplR3PWYp');
                    },
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    "หลังจากนั้นกด \"ถัดไป\" เพื่อใช้งาน",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  ElevatedButton(
                    child: TextApp(
                      text: "ถัดไป",
                      isFontweight: true,
                      color: Colors.white,
                    ),
                    style: ButtonStyle(
                      padding: MaterialStateProperty.all<EdgeInsets>(
                          EdgeInsets.fromLTRB(30, 12, 30, 12)),
                      foregroundColor:
                          MaterialStateProperty.all<Color>(Colors.white),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Color(0xFF39CD07)),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15),
                        ),
                      ),
                    ),
                    onPressed: () async {
                      pref = await SharedPreferences.getInstance();
                      pref.getBool('isUsing');
                      changeScreenCupertino(context, HomePage());
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
