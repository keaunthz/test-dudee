import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_dudee/helpers/utils/screen_navigator.dart';
import 'package:test_dudee/screen/waitWm.dart';
import 'package:test_dudee/widgets/textApp.dart';

class SelectWM extends StatefulWidget {
  final String name;
  const SelectWM({Key? key, required this.name}) : super(key: key);

  @override
  State<SelectWM> createState() => _SelectWMState();
}

class _SelectWMState extends State<SelectWM> {
  Timer? _timer;
  int _start = 0;
  late SharedPreferences pref;

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (_start < 1) {
            timer.cancel();
          } else {
            setState(() {
              _start = _start - 1;
            });
          }
        },
      ),
    );
  }

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }

  Widget build(BuildContext context) {
    MediaQueryData queryData = MediaQuery.of(context);
    double screenWidth = queryData.size.width;
    double screenHeight = queryData.size.height;
    return Scaffold(
      backgroundColor: Color(0xFF12A8D5),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        centerTitle: true,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: TextApp(
          text: "ยืนยันการซักผ้า",
          size: 20,
          isFontweight: true,
        ),
      ),
      body: Column(
        children: [
          Container(
            margin: EdgeInsets.all(15),
            width: screenWidth,
            height: screenHeight * 0.3,
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
            ),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Image.asset(
                    "assets/icon/washingMachine.png",
                    height: 60,
                  ),
                ),
                TextApp(
                  text: widget.name,
                  color: Colors.black,
                  isFontweight: true,
                  size: 18,
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: <Widget>[
                    Flexible(
                      child: TextApp(
                        text:
                            "ระบบจะใช้เวลาในการซักผ้าประมาณ 2 นาที กรุณาใส่เสื้อผ้าและน้ำยาซักผ้าของคุณลงในเครื่องซักให้เรียบร้อย กดปุ่มหยอดเหรียญเพื่อยืนยันการซักผ้า",
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: screenHeight * 0.05,
          ),
          ElevatedButton(
              child: TextApp(
                text: "หยอดเหรียญ",
                isFontweight: true,
              ),
              style: ButtonStyle(
                padding:
                    MaterialStateProperty.all<EdgeInsets>(EdgeInsets.all(12)),
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                backgroundColor:
                    MaterialStateProperty.all<Color>(Color(0xFF39CD07)),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                ),
              ),
              onPressed: () {
                _start = 120;
                startTimer();
                print(_start);
                showDialog(
                  barrierDismissible: false,
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                        elevation: 3,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(20.0),
                          ),
                        ),
                        title: Column(
                          children: [
                            Image.asset(
                              "assets/icon/correct.png",
                              height: 120,
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            TextApp(
                              text: 'การซักผ้าสำเร็จ!',
                              color: Colors.black,
                              isFontweight: true,
                              size: 18,
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            TextApp(
                              text:
                                  'ระบบจะทำการแจ้งเตือนไปยังผู้ใช้บริการ เมื่อมีการซักผ้าใกล้สำเร็จ',
                              color: Colors.black,
                            ),
                          ],
                        ),
                        actions: [
                          TextButton(
                            onPressed: () async {
                              pref = await SharedPreferences.getInstance();
                              pref.setBool("isUsing", true);
                              changeScreenAllReplacementCupertino(
                                  context,
                                  WaitWm(
                                    name: widget.name,
                                    start: _start,
                                  ),
                                  "/");
                            },
                            child: TextApp(
                              text: 'ตกลง',
                              color: Color(0xFF39CD07),
                            ),
                          ),
                        ]);
                  },
                );
              })
        ],
      ),
    );
  }
}
